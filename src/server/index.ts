import express from "express";
import fs from "fs";
import path from "path";
import fetchCatch, { Request } from "./http";
import * as Sentry from "@sentry/node";
import * as Tracing from "@sentry/tracing";


// Settings

const PORT = process.env.PORT ?? 8000;
export const serverConfig = (function() {
	if (!fs.existsSync("config.json")) {
		return {};
	}
	return JSON.parse(fs.readFileSync("config.json").toString());
})();

export const IS_DEBUG = process.env.NODE_ENV !== "production";
export const OWNER_EMAIL = process.env.OWNER_EMAIL ?? serverConfig.OWNER_EMAIL;
const DISCORD_WEBHOOK = process.env.DISCORD_WEBHOOK ?? serverConfig.DISCORD_WEBHOOK;
export const UA_DEFAULT = "Mozilla/5.0 (compatible; Renewed Tab App/1.11.1; +https://renewedtab.com/)";
export const UA_PROXY = "Mozilla/5.0 (compatible; Renewed Tab Proxy/1.11.1; +https://renewedtab.com/)";
const SENTRY_DSN = process.env.SENTRY_DSN;
const SAVE_ROOT = process.env.SAVE_ROOT ?? serverConfig.SAVE_ROOT ?? ".";
export const OPEN_WEATHER_MAP_API_KEY =
	process.env.OPEN_WEATHER_MAP_API_KEY ?? serverConfig.OPEN_WEATHER_MAP_API_KEY;




// App

import { getWeatherInfo } from "./weather";
import { getBackground } from "./backgrounds";
import { handleProxy } from "./proxy";
import { getCoordsFromQuery, getPlaceInfoFromLocation } from "./geocode";
import getImageFromUnsplash from "./backgrounds/unsplash";
import SpaceLaunch from "common/api/SpaceLaunch";
import { getQuote, getQuoteCategories } from "./quotes";
import { getCurrencies } from "./currencies";

const app = express();

Sentry.init({
	enabled: SENTRY_DSN != undefined,
	dsn: SENTRY_DSN,
	integrations: [
		new Sentry.Integrations.Http({ tracing: true }),
		new Tracing.Integrations.Express({ app }),
	],
});

app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use((_req, res, next) => {
	const expiresAt = new Date(new Date().getTime() + 15*60*1000);

	res.append("Access-Control-Allow-Origin", ["*"]);
	res.append("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
	res.append("Access-Control-Allow-Headers", "Content-Type");
	res.append("expires", expiresAt.toISOString());
	next();
});


function writeClientError(res: express.Response, msg: string) {
	res.status(400).type("text").send(msg);
}


import { promRegister, notifyAPIRequest, notifyUpstreamRequest } from "./metrics";
import { preProcessFile } from "typescript";
import { TippyTopImage } from "common/api/icons";

app.get('/metrics', async (req, res) => {
	try {
		const metrics = await promRegister.metrics();
		res.set('Content-Type', promRegister.contentType);
		res.send(metrics);
	} catch (ex: any) {
		res.statusCode = 500;
		res.send(ex.message);
	}
});


app.get("/proxy/", async (req: express.Request, res: express.Response) => {
	if (!req.query.url) {
		writeClientError(res, "Missing URL");
		return;
	}

	notifyAPIRequest("proxy");

	try {
		const url = new URL(req.query.url as string);
		const result = await handleProxy(url);
		res.status(result.status).type(result.contentType).send(result.text);
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


function parseLocation(req: express.Request) {
	if (typeof req.query.long != "string" || typeof req.query.lat != "string") {
		return null;
	}

	const location = {
		latitude: parseFloat(req.query.lat as string),
		longitude: parseFloat(req.query.long as string),
	};

	if (isNaN(location.latitude) || isNaN(location.longitude)) {
		return null;
	}

	return location;
}


app.get("/api/weather/", async (req: express.Request, res: express.Response) => {
	const location = parseLocation(req);
	if (!location) {
		writeClientError(res, "Missing location");
		return;
	}

	notifyAPIRequest("weather");

	try {
		res.json(await getWeatherInfo(location.latitude, location.longitude));
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


app.get("/api/geocode/", async (req: express.Request, res: express.Response) => {
	if (!req.query.q) {
		writeClientError(res, "Missing query");
		return;
	}

	notifyAPIRequest("geocode");

	try {
		res.json(await getCoordsFromQuery((req.query.q as string).trim()));
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


app.get("/api/geolookup/", async (req: express.Request, res: express.Response) => {
	const location = parseLocation(req);
	if (!location) {
		writeClientError(res, "Missing location");
		return;
	}

	notifyAPIRequest("geolookup");

	try {
		res.json(await getPlaceInfoFromLocation(location.latitude, location.longitude));
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


app.get("/api/background/", async (_req: express.Request, res: express.Response) => {
	notifyAPIRequest("background");

	try {
		res.json(await getBackground());
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


const backgroundVoteStream = fs.createWriteStream(
	path.resolve(SAVE_ROOT, "votes.csv"), { flags: "a" });
app.post("/api/background/vote/", async (req: express.Request, res: express.Response) => {
	notifyAPIRequest("vote");

	try {
		const background = req.body.background;
		const isPositive = req.body.is_positive;
		if (background?.id == undefined || isPositive === undefined) {
			writeClientError(res, "Missing background.id or is_positive");
			return;
		}

		const ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
		const url = background.url ?? "";
		const line =
			`${ip}, ${background.id}, ${isPositive ? 'good' : 'bad'}, ${url}\n`;
		backgroundVoteStream.write(line);

		res.json({ success: true });
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


const reCollectionID = /^[A-Za-z0-9]+$/;
app.get("/api/unsplash/", async (req: express.Request, res: express.Response) => {
	notifyAPIRequest("unsplash");

	try {
		const collection = req.query.collection as (string | undefined);
		if (!collection) {
			writeClientError(res, "Missing collection ID");
			return;
		}

		if (!reCollectionID.test(collection)) {
			writeClientError(res, "Invalid collection ID");
			return;
		}

		res.json(await getImageFromUnsplash(collection));
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


app.get("/api/space-flights/", async (_req: express.Request, res: express.Response) => {
	notifyAPIRequest("spaceflights");
	notifyUpstreamRequest("RocketLaunch.live");

	try {
		const ret = await fetchCatch(new Request("https://fdo.rocketlaunch.live/json/launches/next/5", {
			method: "GET",
			size: 0.1 * 1000 * 1000,
			timeout: 10000,
			headers: {
				"User-Agent": UA_DEFAULT,
				"Accept": "application/json",
			},
		}));

		const json = await ret.json();

		// Stupid API keeps changing
		const result = json.response?.result ?? json.result;

		const launches: SpaceLaunch[] = result.map((launch: any) => ({
			id: launch.id,
			name: launch.name,
			provider: launch.provider?.name,
			vehicle: launch.vehicle?.name,
			win_open: launch.win_open,
			win_close: launch.win_close,
			date_str: launch.date_str,
			link: `https://rocketlaunch.live/launch/${launch.slug}`,
		}));

		res.json(launches);
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});

const feedbackStream = fs.createWriteStream(
	path.resolve(SAVE_ROOT, "feedback.txt"), { flags: "a" });
app.post("/api/feedback/", async (req: express.Request, res: express.Response) => {
	notifyAPIRequest("feedback");

	try {
		if (!req.body.event) {
			writeClientError(res, "Missing event");
			return;
		}

		feedbackStream.write(JSON.stringify(req.body) + "\n\n");

		if (DISCORD_WEBHOOK) {
			let comments = req.body.comments;

			const extraIds = [ "missing_features", "difficult", "buggy" ];
			for (const id of extraIds) {
				const value = req.body[`extra-${id}`];
				if (value && value != "") {
					comments += `\n\n${id}:\n${value}`;
				}
			}

			const reasons = (typeof req.body.reason === "string") ? [ req.body.reason ] : req.body.reason;
			let content = `
				**Feedback**
				Event: ${req.body.event}
				Info: ${req.body.version ? "v" + req.body.version : ""} / ${req.body.browser} / ${req.body.platform}
				${reasons ? `Reasons: ${reasons.join(", ")}
						${req.body.other_reason}` : ""}
				${req.body.email ? `Email: ${req.body.email}` : ""}

				${comments}
			`;

			// Only allow at most two new lines in a row, ignoring whitespace
			content = content.replace(/\n\s*\n/g, "\n\n");

			notifyUpstreamRequest("Discord.com");

			await fetchCatch(new Request(DISCORD_WEBHOOK, {
				method: "POST",
				timeout: 10000,
				headers: {
					"User-Agent": UA_DEFAULT,
					"Content-Type": "application/json",
				},
				body: JSON.stringify({
					content: content.replace(/\t/g, "").substr(0, 2000)
				}),
			}));
		}

		if (req.query.r) {
			res.redirect("https://renewedtab.com/feedback/thanks/");
		} else {
			res.json({ success: true });
		}
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


function readAutocompleteFromFile(filename: string) {
	return fs.readFileSync(`src/server/data/${filename}.csv`)
		.toString()
		.split(/\r?\n/)
		.map(x => x.split(","))
		.filter(x => x.length == 2)
		.map(([label, value]) => ({ label: label.trim(), value: value.trim() }))
		.sort((a, b) => a.label.localeCompare(b.label, undefined, { sensitivity: "base" }));
}


const feeds = readAutocompleteFromFile("feeds");
const webcomics = readAutocompleteFromFile("webcomics");
const feeds_background = readAutocompleteFromFile("feeds_background");
app.get("/api/feeds/", async (_req: express.Request, res: express.Response) => {
	notifyAPIRequest("autocomplete:feeds");
	res.json(feeds);
});
app.get("/api/webcomics/", async (_req: express.Request, res: express.Response) => {
	notifyAPIRequest("autocomplete:webcomic");
	res.json(webcomics);
});
app.get("/api/feeds/background/", async (_req: express.Request, res: express.Response) => {
	notifyAPIRequest("autocomplete:feeds_background");
	res.json(feeds_background);
});

app.post("/api/autocomplete/", async (req: express.Request, res: express.Response) => {
	notifyAPIRequest("autocomplete:suggest");
	try {
		if (!req.body.url) {
			writeClientError(res, "Missing URL");
			return;
		}
		if (!DISCORD_WEBHOOK) {
			writeClientError(res, "Server doesn't have suggestions enabled, missing DISCORD_WEBHOOK");
			return;
		}

		const content = `
			**URL Suggestion**
			Url: ${req.body.url}
		`;

		notifyUpstreamRequest("Discord.com");

		await fetchCatch(new Request(DISCORD_WEBHOOK, {
			method: "POST",
			timeout: 10000,
			headers: {
				"User-Agent": UA_DEFAULT,
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				content: content.replace(/\t/g, "").substr(0, 2000)
			}),
		}));

		res.json({ success: true });
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


app.get("/api/quote-categories/", async (req: express.Request, res: express.Response) => {
	notifyAPIRequest("quote-categories");
	try {
		const quoteCategories = await getQuoteCategories();

		res.json(quoteCategories);
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


app.get("/api/quotes/", async (req: express.Request, res: express.Response) => {
	notifyAPIRequest("quotes");
	try {
		let categories: (string[] | undefined);

		const queryArg = req.query.categories;
		if (queryArg instanceof Array) {
			categories = queryArg as string[];
		} else if (typeof queryArg == "string") {
			categories = [ queryArg as string ];
		} else {
			categories = [ "inspire", "life", "love", "funny" ];
		}

		const category = categories[Math.floor(Math.random() * categories.length)];
		const quote = await getQuote(category);
		res.json({ ...quote, category: category });
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});

app.get("/api/currencies/", async (req: express.Request, res: express.Response) => {
	notifyAPIRequest("currency");
	try {
		res.json(await getCurrencies());
	} catch (ex: any) {
		writeClientError(res, ex.message);
	}
});


const TIPPY_TOP_URL = "https://mozilla.github.io/tippy-top-sites/data/icons-top2000.json";
let icons: (TippyTopImage[] | undefined) = undefined;
app.get("/api/website-icons/", async (req, res: express.Response) => {
	if (!icons) {
		const response = await fetchCatch(new Request(TIPPY_TOP_URL), {
			method: "GET",
			timeout: 10000,
			headers: {
				"User-Agent": UA_DEFAULT,
				"Accept": "application/json",
			},
		});
		icons = (await response.json()) as TippyTopImage[];
		icons.push({
			domains: [ "minetest.net", "wiki.minetest.net", "forum.minetest.net" ],
			image_url: "https://www.minetest.net/media/icon.svg",
		});
	}
	res.json(icons);
});


app.use(Sentry.Handlers.errorHandler());

app.listen(PORT, () => {
	console.log(`⚡️[server]: Server is running in ${IS_DEBUG ? "debug" : "prod"} at http://localhost:${PORT}`);
});
